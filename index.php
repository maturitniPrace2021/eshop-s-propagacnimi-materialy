<?php 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


    require_once("src/latte.php");
    $latte = new Latte\Engine;
    $latte->setTempDirectory('temp');
    include("databaseHelper.php");
    include("cookieHelper.php");
    include("headerHelper.php");

    $products = getProducts();
    $featuredProducts = getFeaturedProducts();
    $featuredCategories = getFeaturedCategories();
    $categories = getCategories();

    $headerData = getHeaderData();


    $headerParams = [
        'categories' => $categories,
        'headerData' => $headerData
    ];

    $params = [
        'products' => $products,
        'featuredProducts' => $featuredProducts,
        'featuredCategories' => $featuredCategories
    ];
?>

<!DOCTYPE html>
<html>
<head>
<title>Suvenýry Plzeň</title>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="icon" type="image/png" sizes="32x32" href="http://localhost:8888/plzen-eshop/img/favicon.png">

    <!-- Less -->
<link rel="stylesheet/less" type="text/css" href="styles/style.less" />
<link rel="icon" type="image/png" sizes="32x32" href="http://localhost:8888/plzen-eshop/img/favicon.png">
<script src="//cdn.jsdelivr.net/npm/less@3.13" ></script>

<!-- font -->
<style>
@import url('https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400&display=swap');
</style>

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <title>MainPage</title>
</head>

<body>
    <?php 
        $latte->render('templates/components/headerTemplate.latte', $headerParams); 
        $latte->render('templates/indexTemplate.latte', $params);
        $latte->render('templates/components/footerTemplate.latte');
    ?>
</body>

</html>