<?php
// source: ../templates/objednavka/souhrn.latte

use Latte\Runtime as LR;

class Templatea186e5f47e extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<h1 class="mb-0 text-center my-5 ">SOUHRN</h1>

<div class="container d-flex flex-row justify-content-around rounded bg-black p-4 w-75 mx-auto m-5 border">
    <div class="col-md-4 d-flex flex-column align-items-center my-auto">
        <a href=""
            class=" text-decoration-none mx-auto text-dark text-center mt-3 mt-md-0">Kontaktní údaje</a>
    </div>
    <div class="col-md-4 p-1 bgPrimary d-flex flex-column align-items-center">
        <a href="" class="active text-decoration-none mx-auto text-center" style="color: white !important">Souhrn</a>
    </div>
</div>

<div class="container p-3 p-md-5 my-5 w-75 bg-black rounded mx-auto border rounded">

    <h2 class="text-center">Souhrn objednávky</h2>

    <div class="row mt-5">
        <div class="col-md-6 mx-auto">
            <h4 class="my-3">Osobní údaje</h4>
            <p class="mb-1">
                Jméno a příjmení: <br> <span class="primary"><?php echo LR\Filters::escapeHtmlText($order->get_nameSubject()) /* line 21 */ ?></span>
            </p>
            <p class="mb-1">E-mail: <span class="primary"><?php echo LR\Filters::escapeHtmlText($order->get_email()) /* line 23 */ ?></span></p>
            <p>Telefonní číslo: <span class="primary"><?php echo LR\Filters::escapeHtmlText($order->get_phoneNumber()) /* line 24 */ ?></span></p>
        </div>
        <div class="col-md-6 mx-auto">
            <h4 class="my-3">Fakturační údaje</h4>
            <p class=" mb-1">Adresa: <span class="primary"><?php echo LR\Filters::escapeHtmlText($order->get_address()) /* line 28 */ ?></span></p>
            <p class=" mb-1">Město: <span class="primary"><?php echo LR\Filters::escapeHtmlText($order->get_city()) /* line 29 */ ?></span></p>
            <p>PSČ: <span class="primary"><?php echo LR\Filters::escapeHtmlText($order->get_postalCode()) /* line 30 */ ?></span></p>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-6 mx-auto">
            <!-- if zadané firemní info-->
            <!--
                <h4 class="my-3">Firemní údaje</h4>
                <p class="mb-1">Sídlo: <span class="primary">placeholder</span></p>
                <p class="mb-1">IČO: <span class="primary">placeholder</span></p>
                <p>DIČ: <span class="primary">placeholder</span></p>
                -->
        </div>
    </div>

        <!-- if notes -->
        <hr class="mt-4">

        <h4 class="mt-4">Poznámka k objednávce</h4>
        <p class="mb-5 text-left primary" style="white-space: pre-line">
            <?php echo LR\Filters::escapeHtmlText($order->get_message()) /* line 51 */ ?>

        </p>
    

    <h5 class="pb-2 mt-5">Produkty v košíku</h5>
    <div class="table-responsive">
        <table class="table" style="min-width: 450px;">
            <thead>
            <tr class="">
                <th>Název</th>
                <th>Množství</th>
                <th class="text-right">Cena / ks</th>
                <th class="text-right">Cena</th>
            </tr>
            </thead>
            <tbody>
<?php
		$iterations = 0;
		foreach ($headerData["cartProducts"] as $product) {
?>

                    <tr>
                        <td class="primary"><?php echo LR\Filters::escapeHtmlText($product->get_name()) /* line 70 */ ?></td>
                        <td class="primary"><?php echo LR\Filters::escapeHtmlText($headerData["itemAmounts"][$product->get_id()]) /* line 71 */ ?> ks</td>
                        <td class="primary text-right text-nowrap"><?php echo LR\Filters::escapeHtmlText($product->get_price()) /* line 72 */ ?> Kč/ks</td>
                        <td class="primary text-right text-nowrap"><?php echo LR\Filters::escapeHtmlText($headerData["itemAmounts"][$product->get_id()] * $product->get_price()) /* line 73 */ ?> Kč</td>
                    </tr>

<?php
			$iterations++;
		}
?>
            </tbody>
        </table>
    </div>
            

            <div class="w-100 text-center">
                    <button onclick="finishOrder()" class="btn rounded-0 btn-outline-danger mx-auto"
                            style="cursor:pointer;padding: 5px 20px;">
                        Odeslat objednávku
                    </button>
            </div>
    
</div>

<script>
    function finishOrder() {
            $.ajax({
                type: 'post',
                url: '/plzen-eshop/objednavka/finishOrder.php',
                data: {"send" : true},
                success: function(response) {
                    console.log(response);

                },
                complete: function() {
                    window.location.href = "http://localhost:8888/plzen-eshop/index.php";
                    alert("Děkujeme za objednávku!");
                }
            });
    }
</script><?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['product'])) trigger_error('Variable $product overwritten in foreach on line 67');
		
	}

}
