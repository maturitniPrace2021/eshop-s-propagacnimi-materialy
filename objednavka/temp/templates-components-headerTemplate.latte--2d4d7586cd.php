<?php
// source: ../templates/components/headerTemplate.latte

use Latte\Runtime as LR;

class Template2d4d7586cd extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>

<nav class="navbar fixed-top navbar-expand-md navbar-dark d-none d-lg-block bgColor px-5 mx-0">
   <div class="row px-5 container mx-auto">
      <a class="navbar-brand primary" href="http://localhost:8888/plzen-eshop/index.php" style="margin-left: 15px; font-size: 1.5em;">
            <img class="" style="width: 200px;" src="http://localhost:8888/plzen-eshop/img/tiskovinyPlzeň.png">
      </a>
      <div class="dropdown show my-auto">
      <a class="btn btn-bgColor btn-lg primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         Kategorie
      </a>
      <div class="dropdown-menu bgPrimary" aria-labelledby="dropdownMenuLink">
         <a class="dropdown-item white" href='http://localhost:8888/plzen-eshop/kategorie.php'>Přehled kategorií</a>
<?php
		$iterations = 0;
		foreach ($categories as $category) {
			?>            <a class="dropdown-item white" href='http://localhost:8888/plzen-eshop/kategorie.php?c=<?php
			echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($category->get_id())) /* line 14 */ ?>'><?php echo LR\Filters::escapeHtmlText($category->get_name()) /* line 14 */ ?></a>
<?php
			$iterations++;
		}
?>
      </div>
      </div>
      <input id="productSearch" class="form-control w-25 ml-4 my-auto bgColor rounded-0" type="search" placeholder="Hledání produktů" aria-label="Search">
      <a id="searchButton" onclick="searchProduct()"><img src="http://localhost:8888/plzen-eshop/img/search.svg" class="ml-0 m-0 bgPrimary" style="height: 38px; padding: 7px;"></a>
      
<?php
		if (!isset($preventCartDisplay)) {
?>
      <div id="headerCart" class="row my-auto ml-auto white">
         <a role="button" data-toggle="dropdown" id="cartDropdown" class="d-flex flex-row align-items-center text-light">
            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-cart" viewBox="0 0 16 16">
               <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"></path>
            </svg>
            <div class="bgSecondary my-auto px-2 mx-2 rounded">
            <?php echo LR\Filters::escapeHtmlText($headerData["productCount"]) /* line 28 */ ?>

            </div>
         </a>
         <div id="cartDropdownBody" style="right: 22%; width: 500px; border: none !important; border-radius: 0px !important; max-height: 400px; overflow-y: auto;" class="dropdown-menu keep-open dropdown-menu-right cart shadow" aria-labelledby="cartDropdown">
            <div class="column mx-4">
               <div class="row my-2">
                  <h2 class="my-auto secondary"><?php echo LR\Filters::escapeHtmlText($headerData["productCount"]) /* line 34 */ ?></h2>
                  <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-cart my-auto mx-1 secondary" viewBox="0 0 16 16">
                     <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"></path>
                  </svg>
                  <h3 class="ml-auto secondary my-auto"><?php echo LR\Filters::escapeHtmlText($headerData["totalPrice"]) /* line 38 */ ?></h3>
                  <h3 class="ml-1 secondary my-auto">Kč</h3>
               </div>
               
               
<?php
			if ($headerData["cartProducts"] != null) {
?>
                  
<?php
				$iterations = 0;
				foreach ($headerData["cartProducts"] as $product) {
?>
                  <div class="row p-3 shadow my-4">
                     <a onclick="removeProductFunc(<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($product->get_id())) /* line 47 */ ?>)" class="my-auto" style="text-decoration: none; color: black; cursor: pointer;"><h3>⨉</h3></a>
                     <div class="mx-3" style="width: 80px; height: 80px; <?php
					if ($product->get_imgUrl() == '' || $product->get_imgUrl() == 'unset') {
						?>background-image: url('img/no-image.png');background-repeat: no-repeat;<?php
					}
					else {
						?>background-image: url('storage/<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeCss($product->get_imgUrl())) /* line 48 */ ?>');background-size: cover;<?php
					}
?>  background-position: center;"></div>
                     <div class="d-flex flex-column justify-content-between col-8">
                        <div class="row">
                           <h5 class="mr-auto"><?php echo LR\Filters::escapeHtmlText($product->get_name()) /* line 51 */ ?></h5>
                        </div>
                        <div class="row d-flex flex-row justify-content-between">
                           <p class="my-auto"><?php echo LR\Filters::escapeHtmlText($headerData["itemAmounts"][$product->get_id()] * $product->get_price()) /* line 54 */ ?> Kč</p>
                           <div class="row d-flex flrx-row justify-content-end pl-auto mt-auto mb-0 ml-auto mr-1">
                              <a onclick="addProductFunc(<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($product->get_id())) /* line 56 */ ?>)" style="text-decoration: none; color: black; cursor: pointer;"><h2 class="mb-0 primary">+</h2></a>
                              <h5 class="mb-0 pl-3 my-auto"><?php echo LR\Filters::escapeHtmlText($headerData["itemAmounts"][$product->get_id()]) /* line 57 */ ?></h5>
                              <h5 class="mb-0 pl-1 pr-3 my-auto">ks</h5>
                              <a onclick="subtractProductFunc(<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($product->get_id())) /* line 59 */ ?>)" style="text-decoration: none; color: black; cursor: pointer;"><h2 class="mb-0 primary">-</h2></a>
                           </div>
                        </div>
                     </div>
                  </div>
<?php
					$iterations++;
				}
?>
                  <div class="border mx-auto my-2 px-auto py-2 bgPrimary w-75 white text-center px-auto">
                     <a href="http://localhost:8888/plzen-eshop/kosik.php" class="px-auto white " style="text-decoration: none; color: white;">Pokračovat v objednávce</a>
                  </div>
<?php
			}
			else {
?>
                  <p>Váš nákupní košík je prázdný.</p>
<?php
			}
?>
            </div>
         </div>
      </div>
<?php
		}
		else {
?>
      <div class="ml-auto"></div>
<?php
		}
?>
   </div>
</nav>


<nav style="" class="shadow-sm w-100 mt-0 navbar navbar-expand-lg navbar-dark d-lg-none bgColor fixed-top">
    <div class="container">
        <a class="navbar-brand primary" href="/" style="margin-left: 15px; font-size: 1.5em;">
            <img class="" style="width: 200px;" src="http://localhost:8888/plzen-eshop/img/tiskovinyPlzeň.png">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            
            <ul class="navbar-nav ml-auto py-5">
               <li class="nav-item py-3 display-4 d-flex flex-row">
                  <input id="productSearchMobile" class="form-control w-50 ml-auto my-auto bgColor rounded-0" type="search" placeholder="Hledání produktů" aria-label="Search">
                  <button id="searchButton" onclick="searchProduct()" class="btn rounded-0 m-0 p-0"><img src="http://localhost:8888/plzen-eshop/img/search.svg" class="ml-0 m-0 bgPrimary" style="height: 38px; padding: 7px;"></button>
               </li>
                <li class="nav-item py-3 display-4">
                <div class="row mx-auto">
                    <a class="nav-link text-center ml-auto mr-3" href="http://localhost:8888/plzen-eshop/kategorie.php">
                    <h2 class="">Kategorie</h2>
                    </a>
                    </div>
                </li>
               <li class="nav-item py-3 display-4">
                  <a class="nav-link p-0 mt-2 ml-auto" id="cartDropdown" href="http://localhost:8888/plzen-eshop/kosik.php">
                           <div class="d-flex flex-row align-items-center justify-content-end">
                              <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-cart" viewBox="0 0 16 16">
                                 <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"></path>
                              </svg>
                              <div class="bgSecondary my-auto px-2 mx-2 rounded">
                                 <p class="mb-0" style="font-size: 0.5em;">2</p>
                              </div>
                           </div>
                  </a>
               </li>

                <li class="nav-item" style="height: 100vh;">
                </li>


            </ul>
        </div>
    </div>
</nav>

<div style="height: 64px;"></div>

<script type="text/javascript">
            // Get the input field
      var input = document.getElementById("productSearch");

      // Execute a function when the user releases a key on the keyboard
      input.addEventListener("keyup", function(event) {
      // Number 13 is the "Enter" key on the keyboard
         if (event.keyCode === 13) {
            // Cancel the default action, if needed
            event.preventDefault();
            // Trigger the button element with a click
            document.getElementById("searchButton").click();
         }
      });

      var input = document.getElementById("productSearchMobile");

      // Execute a function when the user releases a key on the keyboard
      input.addEventListener("keyup", function(event) {
      // Number 13 is the "Enter" key on the keyboard
         if (event.keyCode === 13) {
            // Cancel the default action, if needed
            event.preventDefault();
            // Trigger the button element with a click
            document.getElementById("searchButton").click();
         }
      });

      function searchProduct() {
         nameProduct=$("#productSearch").val();
         nameProductMobile=$("#productSearchMobile").val();
         if(nameProduct != "") {
         window.location.href="http://localhost:8888/plzen-eshop/searchResults.php?search="+nameProduct;
         } else if (nameProductMobile != "") {
            window.location.href="http://localhost:8888/plzen-eshop/searchResults.php?search="+nameProductMobile;
         } else {
            console.log("invalid name product");
         }
      }



      function removeProductFunc(productId) {
         $.ajax({
            type: 'get',
            dataType: 'html',
            url: '/plzen-eshop/orderHelper.php',
            data: {"itemId": productId, "removeItem": true},
             success: function() {
               $("#headerCart").load(" #headerCart > *");
            },
            complete: function() {
               document.getElementById("cartDropdownBody").addEventListener("click", function(e) {
                  e.stopPropagation();
               });
            }
         });
      }
      function subtractProductFunc(productId) {
         $.ajax({
            type: 'get',
            dataType: 'html',
            url: '/plzen-eshop/orderHelper.php',
            data: {"itemId": productId, "subtractItem": true},
             success: function() {
               $("#headerCart").load(" #headerCart > *");
            },
            complete: function() {
               document.getElementById("cartDropdownBody").addEventListener("click", function(e) {
                  e.stopPropagation();
               });
            }
         });
      }
      function addProductFunc(productId) {
         $.ajax({
               type: 'get',
               dataType: 'html',
               url: '/plzen-eshop/orderHelper.php',
               data: {"itemId": productId, "addItem": true},
               success: function() {
                  $("#headerCart").load(" #headerCart > *");
               },
               complete: function() {
               document.getElementById("cartDropdownBody").addEventListener("click", function(e) {
                  e.stopPropagation();
               });
            }
         });
      }

      $( document ).ajaxComplete(function() {
         document.getElementById("cartDropdownBody").addEventListener("click", function(e) {
            e.stopPropagation();
         });
      });
      document.getElementById("cartDropdownBody").addEventListener("click", function(e) {
         e.stopPropagation();
      });
      
</script>






<?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['category'])) trigger_error('Variable $category overwritten in foreach on line 13');
		if (isset($this->params['product'])) trigger_error('Variable $product overwritten in foreach on line 45');
		
	}

}
