<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include("../cookieHelper.php");
include("../databaseHelper.php");


if(isset($_POST["send"])) {
    send();
}

function send() {
    $order = getCurrentOrder();

    if($order->is_complete()) {
        $addedId = insertOrder($order->get_items(), $order->get_nameSubject(), $order->get_address(), $order->get_city(), $order->get_postalCode(), $order->get_email(), $order->get_phoneNumber(), $order->get_message());

        echo(json_encode(true));
    } else {
        echo(json_encode(false));
    }
}








?>