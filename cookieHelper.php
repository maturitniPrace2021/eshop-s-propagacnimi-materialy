<?php
    include("CurrentOrder.php");

    
    //Check if cookies granted --------------------------------------------
    setcookie("test_cookie", "test", time() + 3600, '/');
    if(count($_COOKIE) > 0) {
        //echo "<script type='text/javascript'>console.log('Cookies OK');</script>";
    } else {
        //echo "<script type='text/javascript'>alert('Cookies byly zamítnuty. Prosíme o povolení.');</script>";
    }
    //If no current order, create empty one -------------------------------

    if(!isset($_COOKIE["currentOrder"])) {
        $blankOrder = new CurrentOrder(null, null, null, null, null, null, null, null);
        setcookie("currentOrder", json_encode($blankOrder), time()+31536000, "/");
    }

    //---------------------------------------------------------------------
     
    

    function getCurrentOrder() {
        
        $currentOrder = json_decode($_COOKIE["currentOrder"]);
        $currentOrder = new CurrentOrder($currentOrder->nameSubject, $currentOrder->email, $currentOrder->phoneNumber, $currentOrder->address, $currentOrder->city, $currentOrder->postalCode, $currentOrder->message, $currentOrder->items);
        $currentOrder->set_items(json_decode(json_encode($currentOrder->get_items()), true));
        return $currentOrder;
    }

    function setCurrentOrder($order) {
        setcookie("currentOrder", json_encode($order), time()+31536000, "/");
    }


?>