<?php
// source: ../templates/admin/productsEdit.latte

use Latte\Runtime as LR;

class Templatead4a070d27 extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>

<div class="row w-100">
<?php
		$this->renderBlock('adminNav', $this->params, 'html');
?>
    <div class="column col-9">
        <div class="row d-flex flex-row justify-content-between mt-3 mx-3 w-50 mx-auto">
            <img src="../img/tree.svg" class="ml-2" style="width: 30px; opasity: O;">
            <h2 class="text-center">Správa produktů</h2>
            <a href="itemAdd.php">
                <img src="../img/plus.svg" class="ml-3 m-1 bgPrimary rounded" style="width: 30px; height: 30px;">
            </a>
        </div>
        <div class="w-50 mx-auto">
        <hr>
        <div id="productTable">
<?php
		$iterations = 0;
		foreach ($products as $product) {
?>
                <div class="row d-flex flex-row justify-content-between mx-3">
                    <h5 class="my-auto"><?php echo LR\Filters::escapeHtmlText($product->get_name()) /* line 18 */ ?></h5>
                    <div class="row mr-1">
                    <a href="itemEdit.php?productId=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($product->get_id())) /* line 20 */ ?>"><img src="../img/pencil-fill.svg" class="ml-2"></a>
                    <a onclick="" style="cursor: pointer;" data-toggle="modal" data-target="#deleteModal_<?php
			echo LR\Filters::escapeHtmlAttr($product->get_id()) /* line 21 */ ?>"><img src="../img/trash-fill.svg" class="ml-2"></a>
                    </div>
                </div>
                <div class="modal fade" id="deleteModal_<?php echo LR\Filters::escapeHtmlAttr($product->get_id()) /* line 24 */ ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" id="closeModal_<?php echo LR\Filters::escapeHtmlAttr($product->get_id()) /* line 29 */ ?>" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h5 class="text-center">Opravdu chcete smazat produkt "<?php echo LR\Filters::escapeHtmlText($product->get_name()) /* line 34 */ ?>" ?</h5>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Zrušit</button>
                            <button type="button" class="btn btn-danger" onclick="deleteItem(<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($product->get_id())) /* line 38 */ ?>)">Smazat</button>
                        </div>
                        </div>
                    </div>
                </div>
<?php
			$iterations++;
		}
?>
        </div>
        <hr>
        <div>
    </div>
</div>

<script>
    function deleteItem(id) {
            $.ajax({
                type: 'post',
                url: '/plzen-eshop/admin/deleteItem.php',
                data: {"id": id, "send" : true},
                success: function() {
                    $('#closeModal_'+id).click();
                    
                },
                complete: function() {
                $("#productTable").load(" #productTable > *");
                $(".modal-backdrop").hide();

            }
            });
    }
</script><?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['product'])) trigger_error('Variable $product overwritten in foreach on line 16');
		$this->createTemplate("../components/adminNav.latte", $this->params, "import")->render();
		
	}

}
