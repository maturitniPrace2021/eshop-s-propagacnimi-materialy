<?php
// source: ../templates/admin/orderDetail.latte

use Latte\Runtime as LR;

class Template272a8c878e extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>

<div class="row w-100">
<?php
		$this->renderBlock('adminNav', $this->params, 'html');
?>
    <div class="column col-9 ml-auto">
        <div class="row d-flex flex-row justify-content-center mt-3 mx-3 w-50 mx-auto">
            <h2 class="text-center">Přehled objednávek</h2>
        </div>
        <div class="w-50 mx-auto">
        <hr>
        <div class="flex-column d-flex justify-content-between mx-5">
            <div class="column">
                <div class="d-flex flex-row justify-content-between mb-3">
                    <div class="mr-3">
                        <h5 class="">Kontaktní údaje:</h5>
                        <p class="mb-0"><?php echo LR\Filters::escapeHtmlText($order->get_nameSubject()) /* line 16 */ ?></p>
                        <p class="mb-0"><?php echo LR\Filters::escapeHtmlText($order->get_email()) /* line 17 */ ?></p>
                        <p class="mb-0"><?php echo LR\Filters::escapeHtmlText($order->get_phoneNumber()) /* line 18 */ ?></p>
                    </div>
                    <div class="ml-3">
                        <h5 class="">Adresa:</h5>
                        <p class="mb-0"><?php echo LR\Filters::escapeHtmlText($order->get_address()) /* line 22 */ ?></p>
                        <p class="mb-0"><?php echo LR\Filters::escapeHtmlText($order->get_city()) /* line 23 */ ?> <?php
		echo LR\Filters::escapeHtmlText($order->get_postalCode()) /* line 23 */ ?></p>
                    </div>
                </div>
                <div class="mb-3">
                    <h5 class="">Poznámka:</h5>
                    <p class="mb-0"><?php echo LR\Filters::escapeHtmlText($order->get_message()) /* line 28 */ ?></p>
                </div>
            </div>
            <div class="column flex-column d-flex justify-content-between">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Název produktu</th>
                            <th scope="col">Kusů</th>
                            <th scope="col">Cena za kus</th>
                            <th scope="col">Cena celkem</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
		$iterations = 0;
		foreach ($products as $product) {
?>
                            <tr>
                                <th scope="row"><?php echo LR\Filters::escapeHtmlText($product->get_id()) /* line 45 */ ?></th>
                                <td><?php echo LR\Filters::escapeHtmlText($product->get_name()) /* line 46 */ ?></td>
                                <td><?php echo LR\Filters::escapeHtmlText($itemAmounts[$product->get_id()]) /* line 47 */ ?></td>
                                <td><?php echo LR\Filters::escapeHtmlText($product->get_price()) /* line 48 */ ?> Kč</td>
                                <td><?php echo LR\Filters::escapeHtmlText($itemAmounts[$product->get_id()] * $product->get_price()) /* line 49 */ ?> Kč</td>
                            </tr>
<?php
			$iterations++;
		}
?>
                    </tbody>
                </table>
                <p class="text-right">Cena celkem: <?php echo LR\Filters::escapeHtmlText($order->get_totalPrice()) /* line 54 */ ?> Kč</p>
            </div>
        </div>
</div>

<script>
</script><?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['product'])) trigger_error('Variable $product overwritten in foreach on line 43');
		$this->createTemplate("../components/adminNav.latte", $this->params, "import")->render();
		
	}

}
