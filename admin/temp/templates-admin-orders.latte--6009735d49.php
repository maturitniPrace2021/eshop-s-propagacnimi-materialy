<?php
// source: ../templates/admin/orders.latte

use Latte\Runtime as LR;

class Template6009735d49 extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>

<div class="row w-100">
<?php
		$this->renderBlock('adminNav', $this->params, 'html');
?>
    <div class="column col-9 ml-auto">
        <div class="row d-flex flex-row justify-content-center mt-3 mx-3 w-50 mx-auto">
            <h2 class="text-center">Přehled objednávek</h2>
        </div>
        <div class="w-50 mx-auto">
        <hr class="mb-0">
        <div>
<?php
		$iterations = 0;
		foreach ($orders as $order) {
			?>            <a href="orderDetail.php?orderId=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($order->get_id())) /* line 13 */ ?>" style="cursor: pointer">
                <div class="nav-button p-2 flex-row d-flex justify-content-between align-items-center">
                    <p class="mb-0"><?php echo LR\Filters::escapeHtmlText($order->get_id()) /* line 15 */ ?></p>
                    <p class="mb-0 mx-3"><?php echo LR\Filters::escapeHtmlText($order->get_nameSubject()) /* line 16 */ ?></p>
                    <p class="mb-0 ml-auto"><?php echo LR\Filters::escapeHtmlText($order->get_totalPrice()) /* line 17 */ ?> Kč</p>
                </div>
            </a>
            <hr class="m-0">
<?php
			$iterations++;
		}
?>
        </div>
</div>

<script>
</script><?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['order'])) trigger_error('Variable $order overwritten in foreach on line 12');
		$this->createTemplate("../components/adminNav.latte", $this->params, "import")->render();
		
	}

}
