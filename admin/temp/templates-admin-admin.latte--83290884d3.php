<?php
// source: ../templates/admin/admin.latte

use Latte\Runtime as LR;

class Template83290884d3 extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>

<div class="row w-100">
<?php
		$this->renderBlock('adminNav', $this->params, 'html');
?>
    <div class="column col-9 ml-auto">
        <h2 class="text-center mt-3">Hlavní stránka</h2>
        <div class="w-50 mx-auto">
            <hr>
            <h4 class="primary ml-3">Zvýrazněné produkty</h4>
            <div id="productTable">
<?php
		$iterations = 0;
		foreach ($featuredProducts as $product) {
?>
                    <div class="d-flex flex-row justify-content-between">
                        <h5 class="my-auto"><?php echo LR\Filters::escapeHtmlText($product->get_name()) /* line 13 */ ?></h5>
                        <a onclick="removeFeaturedProduct(<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($product->get_id())) /* line 14 */ ?>)" style="cursor: pointer;" class=""><img src="../img/trash-fill.svg" class="ml-2"></a>
                    </div>
<?php
			$iterations++;
		}
?>
            </div>
            <div class="d-flex flex-row justify-content-between my-3">
                <select id="product" class="my-auto">
                    <option value="">Vyberte produkt:</option>
<?php
		$iterations = 0;
		foreach ($categories as $category) {
			?>                        <optgroup label="<?php echo LR\Filters::escapeHtmlAttr($category->get_name()) /* line 22 */ ?>">
<?php
			$iterations = 0;
			foreach ($products as $product) {
				if ($category->get_id() == $product->get_category()) {
					?>                                    <option value="<?php echo LR\Filters::escapeHtmlAttr($product->get_id()) /* line 25 */ ?>"><?php
					echo LR\Filters::escapeHtmlText($product->get_name()) /* line 25 */ ?></option>
<?php
				}
				$iterations++;
			}
?>
                        </optgroup>
<?php
			$iterations++;
		}
?>
                </select>
                <button onclick="addFeaturedProduct()" class="btn rounded-0 bgPrimary py-1 white text-center px-3">Přidat produkt</button>
            </div>
            <hr>
            <h4 class="primary ml-3">Zvýrazněné kategorie</h4>
            <div id="categoryTable">
<?php
		$iterations = 0;
		foreach ($featuredCategories as $category) {
?>
                    <div class="d-flex flex-row justify-content-between">
                        <h5 class="my-auto"><?php echo LR\Filters::escapeHtmlText($category->get_name()) /* line 38 */ ?></h5>
                        <a onclick="removeFeaturedCategory(<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($category->get_id())) /* line 39 */ ?>)" style="cursor: pointer;" class=""><img src="../img/trash-fill.svg" class="ml-2"></a>
                    </div>
<?php
			$iterations++;
		}
?>
            </div>
            <div class="d-flex flex-row justify-content-between my-3">
                <select id="category" class="my-auto">
                    <option value="">Vyberte kategorii:</option>
<?php
		$iterations = 0;
		foreach ($categories as $category) {
			?>                        <option value="<?php echo LR\Filters::escapeHtmlAttr($category->get_id()) /* line 47 */ ?>"><?php
			echo LR\Filters::escapeHtmlText($category->get_name()) /* line 47 */ ?></option>
<?php
			$iterations++;
		}
?>
                </select>
                <button onclick="addFeaturedCategory()" class="btn rounded-0 bgPrimary py-1 white text-center px-3">Přidat kategorii</button>
            </div>
            <hr>
        </div>
    </div>
</div>

<script>
    function addFeaturedCategory() {
        id = $("#category").val();
        $.ajax({
                type: 'post',
                url: '/plzen-eshop/admin/featured.php',
                data: {"category": true, "id": id, "add": true, "send" : true},
                success: function(response) {
                    $("#categoryTable").load(" #categoryTable > *");
                },
                complete: function() {
                
            }
        });
    }

    function addFeaturedProduct() {
        id = $("#product").val();
        $.ajax({
                type: 'post',
                url: '/plzen-eshop/admin/featured.php',
                data: {"product": true, "id": id, "add": true, "send" : true},
                success: function(response) {
                    $("#productTable").load(" #productTable > *");
                },
                complete: function() {
                
            }
        });
    }

    function removeFeaturedCategory(id) {
        $.ajax({
                type: 'post',
                url: '/plzen-eshop/admin/featured.php',
                data: {"category": true, "id": id, "remove": true, "send" : true},
                success: function(response) {
                    $("#categoryTable").load(" #categoryTable > *");
                },
                complete: function() {
                
            }
        });
    }

    function removeFeaturedProduct(id) {
        $.ajax({
                type: 'post',
                url: '/plzen-eshop/admin/featured.php',
                data: {"product": true, "id": id, "remove": true, "send" : true},
                success: function(response) {
                    $("#productTable").load(" #productTable > *");
                },
                complete: function() {
                
            }
        });
    }
</script><?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['product'])) trigger_error('Variable $product overwritten in foreach on line 11, 23');
		if (isset($this->params['category'])) trigger_error('Variable $category overwritten in foreach on line 21, 36, 46');
		$this->createTemplate("../components/adminNav.latte", $this->params, "import")->render();
		
	}

}
