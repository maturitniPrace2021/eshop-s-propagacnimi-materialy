<?php
// source: ../templates/admin/storage.latte

use Latte\Runtime as LR;

class Template175f582edc extends Latte\Runtime\Template
{
	public $blocks = [
		'scripts' => 'blockScripts',
	];

	public $blockTypes = [
		'scripts' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<div class="row w-100">
<?php
		$this->renderBlock('adminNav', $this->params, 'html');
?>
    <div class="column col-9">
        <h2 class="text-center">Správa skladu</h2>
        <div class="w-50 mx-auto" id="productTable">
            <hr>
<?php
		$iterations = 0;
		foreach ($products as $product) {
?>
                <div class="row d-flex flex-row justify-content-between mx-3">
                    <h5 class="my-auto"><?php echo LR\Filters::escapeHtmlText($product->get_name()) /* line 11 */ ?></h5>
                    <div class="row d-flex flex-row justify-content-end">
                        <h5 class="my-auto edit0" id="edit_0_<?php echo LR\Filters::escapeHtmlAttr($product->get_id()) /* line 13 */ ?>" style="display: flex;"><?php
			echo LR\Filters::escapeHtmlText($product->get_inStock()) /* line 13 */ ?> ks</h5>
                        <input type="text" class="form-control my-1 w-25 edit1" id="edit_1_<?php echo LR\Filters::escapeHtmlAttr($product->get_id()) /* line 14 */ ?>" value="<?php
			echo LR\Filters::escapeHtmlAttr($product->get_inStock()) /* line 14 */ ?>" style="display: none;">
                        <a onclick="edit(<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($product->get_id())) /* line 15 */ ?>)" class="edit0" id="edit_2_<?php
			echo LR\Filters::escapeHtmlAttr($product->get_id()) /* line 15 */ ?>" style="display: flex;"><img src="../img/pencil-fill.svg" class="ml-2"></a>
                        <a onclick="save(<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($product->get_id())) /* line 16 */ ?>)" class="edit1" id="edit_3_<?php
			echo LR\Filters::escapeHtmlAttr($product->get_id()) /* line 16 */ ?>" style="display: none;"><img src="../img/check2.svg" class="ml-2"></a>
                        <a onclick="cancel(<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($product->get_id())) /* line 17 */ ?>)" class="edit1" id="edit_4_<?php
			echo LR\Filters::escapeHtmlAttr($product->get_id()) /* line 17 */ ?>" style="display: none;"><img src="../img/x.svg" class="ml-2"></a>
                    </div>
                </div>
<?php
			$iterations++;
		}
?>
            <hr>
        </div>
    </div>
</div>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('scripts', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['product'])) trigger_error('Variable $product overwritten in foreach on line 9');
		$this->createTemplate("../components/adminNav.latte", $this->params, "import")->render();
		
	}


	function blockScripts($_args)
	{
		extract($_args);
?>
<script>
    function edit(id) {
        document.getElementById("edit_0_" + id).style.display = "none";
        document.getElementById("edit_1_" + id).style.display = "flex";
        document.getElementById("edit_2_" + id).style.display = "none";
        document.getElementById("edit_3_" + id).style.display = "flex";
        document.getElementById("edit_4_" + id).style.display = "flex";
    }

    function save(id) {
        newCount = $("#edit_1_"+id).val();
                $.ajax({
            type: 'post',
            url: '/plzen-eshop/admin/editItemCount.php',
            data: {"id": id, "newCount": newCount, "send" : true},
            success: function(response) {
                console.log(response);
                if(JSON.parse(response) == true) {
                    document.getElementById("edit_0_" + id).style.display = "flex";
                    document.getElementById("edit_1_" + id).style.display = "none";
                    document.getElementById("edit_2_" + id).style.display = "flex";
                    document.getElementById("edit_3_" + id).style.display = "none";
                    document.getElementById("edit_4_" + id).style.display = "none";
                    $("#productTable").load(" #productTable > *");
                } else {
                    console.log("invalid value");
                }
            },
            complete: function() {
            
            }
        });

    }

    function cancel(id) {
        document.getElementById("edit_0_" + id).style.display = "flex";
        document.getElementById("edit_1_" + id).style.display = "none";
        document.getElementById("edit_2_" + id).style.display = "flex";
        document.getElementById("edit_3_" + id).style.display = "none";
        document.getElementById("edit_4_" + id).style.display = "none";
    }
</script>
<?php
	}

}
