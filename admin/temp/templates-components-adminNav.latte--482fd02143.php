<?php
// source: ..\templates\components\adminNav.latte

use Latte\Runtime as LR;

class Template482fd02143 extends Latte\Runtime\Template
{
	public $blocks = [
		'adminNav' => 'blockAdminNav',
	];

	public $blockTypes = [
		'adminNav' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		return get_defined_vars();
	}


	function blockAdminNav($_args)
	{
		extract($_args);
?>
     <div class="d-flex flex-column justify-content-between col-3 fixed-left px-5 shadow fixed-bottom" style="height: 100vh;">
        <div class="">
            <a href="../index.php"><h4 class="text-center primary my-3">Administrace E-Shopu</h4><a>
            <hr>
            <ul class="nav flex-column">
                <li class="nav-item nav-button my-1 <?php
		if (($active == 1)) {
			?>active<?php
		}
?>"><a href="index.php"><h5 class="nav-button my-auto py-2 pl-3">Hlavní stránka</h5></a></li>
                <li class="nav-item nav-button my-1 <?php
		if (($active == 2)) {
			?>active<?php
		}
?>"><a href="productsEdit.php"><h5 class="nav-button my-auto py-2 pl-3">Správa produktů</h5></a></li>
                <li class="nav-item nav-button my-1 <?php
		if (($active == 3)) {
			?>active<?php
		}
?>"><a href="categoryEdit.php"><h5 class="nav-button my-auto py-2 pl-3">Správa kategorií</h5></a></li>
                <li class="nav-item nav-button my-1 <?php
		if (($active == 4)) {
			?>active<?php
		}
?>"><a href="storage.php"><h5 class="nav-button my-auto py-2 pl-3">Správa skladu</h5></a></li>
                <li class="nav-item nav-button my-1 <?php
		if (($active == 5)) {
			?>active<?php
		}
?>"><a href="orders.php"><h5 class="nav-button my-auto py-2 pl-3">Přehled objednávek</h5></a></li>
            </ul>
        </div>
    </div>
<?php
	}

}
