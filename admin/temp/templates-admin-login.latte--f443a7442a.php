<?php
// source: ../templates/admin/login.latte

use Latte\Runtime as LR;

class Templatef443a7442a extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<div class="w-100">
    <form action="" method="POST">
        <div style="height: 100px; visibility: hidden;"></div>
        <div class="d-flex flex-row justify-content-center" style="visibility: visible;">
            <a href="http://localhost:8888/plzen-eshop/">
                <img class="mx-auto" style="width: 400px;" src="http://localhost:8888/plzen-eshop/img/tiskovinyPlzeň.png">
            </a>
        </div>
        <div style="height: 100px; visibility: hidden;"></div>
        <div class="mx-auto w-50 d-flex flex-column justify-content-center p-5 h-100" style="background-color: white">
            <h1 class="text-center">Přihlášení do administrace</h1>
            <hr>
            <div class="d-flex flex-row justify-content-between">
                <label for="name" class="my-auto">Přihlašovácí jméno:</label>
                <input type="text" class="form-control rounded-0 my-1 w-50" name="name" placeholder="Jméno">
            </div>
            <div class="d-flex flex-row justify-content-between">
                <label for="password" class="my-auto">Heslo:</label>
                <input type="password" class="form-control rounded-0 my-1 w-50" name="password" placeholder="*****">
            </div>
            <button type="submit" class="btn bgPrimary white rounded-0 mx-auto w-25 mt-5" name="submit">Přihlásit se</button>
        </div>
    </form>
</div>

<script>
<?php
		if ($loggedIn == true) {
?>
        window.location.href="http://localhost:8888/plzen-eshop/admin/index.php";
<?php
		}
		?></script><?php
		return get_defined_vars();
	}

}
