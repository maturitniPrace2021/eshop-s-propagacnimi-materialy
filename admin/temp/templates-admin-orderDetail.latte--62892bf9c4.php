<?php
// source: ../templates/admin/orderDetail.latte

use Latte\Runtime as LR;

class Template62892bf9c4 extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>

<div class="row w-100">
<?php
		$this->renderBlock('adminNav', $this->params, 'html');
?>
    <div class="column col-9">
        <div class="row d-flex flex-row justify-content-center mt-3 mx-3 w-50 mx-auto">
            <h2 class="text-center">Přehled objednávek</h2>
        </div>
        <div class="w-50 mx-auto">
        <hr>
        <div class="flex-column d-flex justify-content-between mx-5">
            <div class="column">
                <div class="d-flex flex-row mb-3">
                    <div class="mr-3">
                        <h5 class="">Kontaktní údaje:</h5>
                        <p class="mb-0"><?php echo LR\Filters::escapeHtmlText($order->get_nameSubject()) /* line 16 */ ?></p>
                        <p class="mb-0"><?php echo LR\Filters::escapeHtmlText($order->get_email()) /* line 17 */ ?></p>
                        <p class="mb-0"><?php echo LR\Filters::escapeHtmlText($order->get_phoneNumber()) /* line 18 */ ?></p>
                    </div>
                    <div class="mr-3">
                        <h5 class="">Adresa:</h5>
                        <p class="mb-0"><?php echo LR\Filters::escapeHtmlText($order->get_address()) /* line 22 */ ?></p>
                        <p class="mb-0"><?php echo LR\Filters::escapeHtmlText($order->get_city()) /* line 23 */ ?> <?php
		echo LR\Filters::escapeHtmlText($order->get_postalCode()) /* line 23 */ ?></p>
                    </div>
                </div>
            </div>
            <div class="column flex-column d-flex justify-content-between">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Název produktu</th>
                            <th scope="col">Kusů</th>
                            <th scope="col">Cena za kus</th>
                            <th scope="col">Cena celkem</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
		$iterations = 0;
		foreach ($products as $product) {
?>
                            <tr>
                                <th scope="row"><?php echo LR\Filters::escapeHtmlText($product->get_id()) /* line 41 */ ?></th>
                                <td><?php echo LR\Filters::escapeHtmlText($product->get_name()) /* line 42 */ ?></td>
                                <td><?php echo LR\Filters::escapeHtmlText($itemAmounts[$product->get_id()]) /* line 43 */ ?></td>
                                <td><?php echo LR\Filters::escapeHtmlText($product->get_price()) /* line 44 */ ?> Kč</td>
                                <td><?php echo LR\Filters::escapeHtmlText($itemAmounts[$product->get_id()] * $product->get_price()) /* line 45 */ ?> Kč</td>
                            </tr>
<?php
			$iterations++;
		}
?>
                    </tbody>
                </table>
                <p class="text-right">Cena celkem: <?php echo LR\Filters::escapeHtmlText($order->get_totalPrice()) /* line 50 */ ?> Kč</p>
            </div>
        </div>
</div>

<script>
</script><?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['product'])) trigger_error('Variable $product overwritten in foreach on line 39');
		$this->createTemplate("../components/adminNav.latte", $this->params, "import")->render();
		
	}

}
