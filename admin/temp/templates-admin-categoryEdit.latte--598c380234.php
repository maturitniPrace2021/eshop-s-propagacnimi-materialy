<?php
// source: ../templates/admin/categoryEdit.latte

use Latte\Runtime as LR;

class Template598c380234 extends Latte\Runtime\Template
{
	public $blocks = [
		'scripts' => 'blockScripts',
	];

	public $blockTypes = [
		'scripts' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<div class="row w-100">
<?php
		$this->renderBlock('adminNav', $this->params, 'html');
?>
    <div class="column col-9 ml-auto">
        <div class="row d-flex flex-row justify-content-between mt-3 mx-3 w-50 mx-auto">
            <img src="../img/tree.svg" class="ml-2" style="width: 30px; opasity: O;">
            <h2 class="text-center">Správa kategorií</h2>
            <a onclick="newCat(); cancel()" style="cursor: pointer;">
                <img src="../img/plus.svg" class="ml-3 m-1 bgPrimary rounded" style="width: 30px; height: 30px;">
            </a>
        </div>
        <div class="w-50 mx-auto" id="categoryTable">
            <hr>
<?php
		$iterations = 0;
		foreach ($categories as $category) {
?>
                <div class="row d-flex flex-row justify-content-between mx-3 my-3">
                    <div class="rename0" id="rename_0_<?php echo LR\Filters::escapeHtmlAttr($category->get_id()) /* line 17 */ ?>"><h5 class="my-auto" style="flex"><?php
			echo LR\Filters::escapeHtmlText($category->get_name()) /* line 17 */ ?></h5></div>
                    <input type="text" class="form-control w-50 rename1" id="rename_1_<?php echo LR\Filters::escapeHtmlAttr($category->get_id()) /* line 18 */ ?>" value="<?php
			echo LR\Filters::escapeHtmlAttr($category->get_name()) /* line 18 */ ?>" style="display: none;">
                    <div class="row mr-1">
                    <a onclick="rename(<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($category->get_id())) /* line 20 */ ?>);" id="rename_2_<?php
			echo LR\Filters::escapeHtmlAttr($category->get_id()) /* line 20 */ ?>" class="rename0" style="cursor: pointer; display: flex;"><img src="../img/pencil-fill.svg" class="ml-2"></a>
                    <a onclick="save(<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($category->get_id())) /* line 21 */ ?>)" id="rename_3_<?php
			echo LR\Filters::escapeHtmlAttr($category->get_id()) /* line 21 */ ?>" class="rename1" style="cursor: pointer; display: none;"><img src="../img/check2.svg" class="ml-2"></a>
                    <a onclick="" style="cursor: pointer; display: flex;" id="rename_4_<?php echo LR\Filters::escapeHtmlAttr($category->get_id()) /* line 22 */ ?>" class="rename0" data-toggle="modal" data-target="#deleteModal_<?php
			echo LR\Filters::escapeHtmlAttr($category->get_id()) /* line 22 */ ?>"><img src="../img/trash-fill.svg" class="ml-2"></a>
                    <a onclick="cancel(<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($category->get_id())) /* line 23 */ ?>)" id="rename_5_<?php
			echo LR\Filters::escapeHtmlAttr($category->get_id()) /* line 23 */ ?>" class="rename1" style="cursor: pointer; display: none;"><img src="../img/x.svg" class="ml-2"></a>
                    </div>
                </div>

                <div class="modal fade" id="deleteModal_<?php echo LR\Filters::escapeHtmlAttr($category->get_id()) /* line 27 */ ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" id="closeModal_<?php echo LR\Filters::escapeHtmlAttr($category->get_id()) /* line 32 */ ?>" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h5 class="text-center">Opravdu chcete smazat produkt "<?php echo LR\Filters::escapeHtmlText($category->get_name()) /* line 37 */ ?>" ?</h5>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Zrušit</button>
                            <button onclick="remove(<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($category->get_id())) /* line 41 */ ?>)" type="button" class="btn btn-danger" data-dismiss="modal" >Smazat</button>
                        </div>
                        </div>
                    </div>
                </div>
<?php
			$iterations++;
		}
?>
            <hr>
            <div class="row d-flex flex-row justify-content-between mx-3 my-3">
                <input type="text" id="newCategory" class="form-control w-50 new" placeholder="Název nové kategorie" style="display: none;">
                <div class="row mr-1">
                    <a onclick="submit()" class="new" style="cursor: pointer; display: none;"><img src="../img/check2.svg" class="ml-2"></a>
                    <a onclick="cancel1()" class="new" style="cursor: pointer; display: none;"><img src="../img/x.svg" class="ml-2"></a>
                </div>
            </div>
        <div>
    </div>
</div>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('scripts', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['category'])) trigger_error('Variable $category overwritten in foreach on line 15');
		$this->createTemplate("../components/adminNav.latte", $this->params, "import")->render();
		
	}


	function blockScripts($_args)
	{
		extract($_args);
?>
<script>

    function rename(id) {
            document.getElementById("rename_0_" + id).style.display = "none";
            document.getElementById("rename_1_" + id).style.display = "flex";
            document.getElementById("rename_2_" + id).style.display = "none";
            document.getElementById("rename_3_" + id).style.display = "flex";
            document.getElementById("rename_4_" + id).style.display = "none";
            document.getElementById("rename_5_" + id).style.display = "flex";
    }

    function save(id) {
        editCategoryName = $("#rename_1_"+id).val();
        $.ajax({
            type: 'post',
            url: '/plzen-eshop/admin/categoryRename.php',
            data: {"id": id, "name": editCategoryName, "send" : true},
            success: function(response) {
                console.log(response);
                if(JSON.parse(response) == true) {
                    document.getElementById("rename_0_" + id).style.display = "flex";
                    document.getElementById("rename_1_" + id).style.display = "none";
                    document.getElementById("rename_2_" + id).style.display = "flex";
                    document.getElementById("rename_3_" + id).style.display = "none";
                    document.getElementById("rename_4_" + id).style.display = "flex";
                    document.getElementById("rename_5_" + id).style.display = "none";
                    $("#categoryTable").load(" #categoryTable > *");
                } else {
                    console.log("empty field");
                }
            },
            complete: function() {
            
            }
        });
    }

    function remove(id) {
        $.ajax({
            type: 'post',
            url: '/plzen-eshop/admin/categoryDelete.php',
            data: {"id": id, "send" : true},
            success: function(response) {
                console.log(response);
                if(JSON.parse(response) == true) {
                    $("#categoryTable").load(" #categoryTable > *");
                } else {
                    console.log("empty field");
                }
                $('#closeModal_'+id).click();
            },
            complete: function() {
                $(".modal-backdrop").hide();
            }
        });
    }

    function cancel(id) {
        document.getElementById("rename_0_" + id).style.display = "flex";
        document.getElementById("rename_1_" + id).style.display = "none";
        document.getElementById("rename_2_" + id).style.display = "flex";
        document.getElementById("rename_3_" + id).style.display = "none";
        document.getElementById("rename_4_" + id).style.display = "flex";
        document.getElementById("rename_5_" + id).style.display = "none";
    }

    function newCat() {
        $(".new").show();
    }

    function submit() {
        newCategoryName = $("#newCategory").val();
        $.ajax({
            type: 'post',
            url: '/plzen-eshop/admin/categoryAdd.php',
            data: {"name": newCategoryName, "send" : true},
            success: function(response) {
                console.log(response);
                if(JSON.parse(response) == true) {
                    $(".new").hide();
                    $("#categoryTable").load(" #categoryTable > *");
                } else {
                    console.log("empty field");
                }
            },
            complete: function() {
            
            }
        });
    }

    function cancel1() {
        $(".new").hide();
    }
    
</script>
<?php
	}

}
