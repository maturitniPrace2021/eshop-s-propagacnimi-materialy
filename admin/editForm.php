<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include("../cookieHelper.php");
include("../databaseHelper.php");


if(isset($_POST["send"])) {
    send();
}

   function send() {
    $name = null;
    $description = null;
    $category = null;
    $price = null;
    $inStock = null;
    $imgUrl = "";


    $errors = [];

    if(trim($_POST["name"]) == null) {
        $errors[] = "Jméno produktu je povinné pole.";
    } else {
        $name = htmlspecialchars($_POST["name"]);
    }

    if(trim($_POST["description"]) == null) {
        $errors[] = "Popis produktu je povinné pole.";
    } else {
        $description = htmlspecialchars($_POST["description"]);
    }

    if(trim($_POST["category"]) == null) {
        $errors[] = "Kategorie produktu je povinné pole.";
    } else {
        $category = htmlspecialchars($_POST["category"]);
    }

    if(trim($_POST["price"]) == null) {
        $errors[] = "Cena produktu je povinné pole.";
    } else {
        $price = htmlspecialchars($_POST["price"]);
    }

    if(trim($_POST["inStock"]) == null) {
        $errors[] = "Počet skladem povinné pole.";
    } else {
        $inStock = htmlspecialchars($_POST["inStock"]);
    }





    if(!empty($errors)) {
        echo json_encode($errors);
    } else {
        if(isset($_POST["edit"])) {
            if($_POST["deleteImg"]) {
                removeProductImg($_POST["id"]);
            }
            $result = updateProduct($name, $description, $category, $price, $inStock, $_POST["id"]);
            echo(json_encode($result));
        } else {
            $result = insertProduct($name, $description, $category, $price, $inStock, $imgUrl);
            echo(json_encode($result));
        }
    
    }
}








?>