<?php
include("../databaseHelper.php");
function getFileType() {
    $filename = $_FILES["image"]["name"];
    echo $filename;
    echo("<br>");
    $extension = substr($filename, strpos($filename, "."), strlen($filename)-1);
    //echo $extension;
    return $extension;
}

print_r($_FILES["image"]);

$target_dir = "../storage/";
$target_file = $target_dir . $_POST["productId"] . getFileType();
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        echo("<br>");
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        echo("<br>");
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size if smaller than 10MB
if ($_FILES["image"]["size"] > 10000000) {
    echo "Sorry, your file is too large.";
    echo("<br>");
    $uploadOk = 0;
}
// Allow certain file formats
if(exif_imagetype($_FILES["image"]["tmp_name"]) < 1 && $_FILES["image"]["tmp_name"] > 4) {
    echo "Sorry, only JPG, JPEG, PNG files are allowed.";
    echo("<br>");
    $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
    echo("<br>");
  // if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
      echo "The file ". htmlspecialchars( basename( $_FILES["image"]["name"])). " has been uploaded.";
      echo("<br>");
      updateProductImg($_POST["productId"], getFileType());
      echo('<script>window.location.href="http://localhost:8888/plzen-eshop/admin/productsEdit.php";</script>');
    } else {
      echo "Sorry, there was an error uploading your file.";
      echo("<br>");
    }
}
?>