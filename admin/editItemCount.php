<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include("../cookieHelper.php");
include("../databaseHelper.php");


if(isset($_POST["send"])) {
    send();
}

function send() {
    $id = htmlspecialchars($_POST["id"]);
    $newCount = htmlspecialchars($_POST["newCount"]);

    if($newCount >= 0) {
        updateItemCount($id, $newCount);
        echo(json_encode(true));
    }
}
?>