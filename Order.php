<?php

class Order {
    public $id;
    public $totalPrice;
    public $nameSubject;
    public $email;
    public $phoneNumber;
    public $address;
    public $city;
    public $postalCode;
    public $message;
    public $items;

    public function __construct($id, $totalPrice, $nameSubject, $email, $phoneNumber, $address, $city, $postalCode, $message, $items){
        $this->id = $id;
        $this->totalPrice = $totalPrice;
        $this->nameSubject = $nameSubject;
        $this->email = $email;
        $this->phoneNumber = $phoneNumber;
        $this->address = $address;
        $this->city = $city;
        $this->postalCode = $postalCode;
        $this->message = $message;
        $this->items = $items;
    }
    function get_id() {
        return $this->id;
    }

    function get_nameSubject() {
        return $this->nameSubject;
    }

    function get_totalPrice() {
        return $this->totalPrice;
    }

    function get_email() {
        return $this->email;
    }
    function get_phoneNumber() {
        return $this->phoneNumber;
    }

    function get_address() {
        return $this->address;
    }

    function get_city() {
        return $this->city;
    }

    function get_postalCode() {
        return $this->postalCode;
    }

    function get_message() {
        return $this->message;
    }

    function get_items() {
        return $this->items;
    }

    function set_nameSubject($nameSubject) {
        $this->nameSubject = $nameSubject;
    }

    function set_email($email) {
        $this->email = $email;
    }

    function set_phoneNumber($phoneNumber) {
        $this->phoneNumber = $phoneNumber;
    }

    function set_address($address) {
        $this->address = $address;
    }

    function set_city($city) {
        $this->city = $city;
    }

    function set_postalCode($postalCode) {
        $this->postalCode = $postalCode;
    }

    function set_message($message) {
        $this->message = $message;
    }

    function set_items($items) {
        $this->items = $items;
    }

    function get_product_count() {
        if($this->items != null) {
            return count(array_keys($this->items));
        } else {
            return 0;
        }
    }

    function get_product_ids() {
        if($this->items != null) {
            return array_keys($this->items);
        } else {return null;}
    }

    function is_complete() {
        if( $this->nameSubject != null &&
            $this->address != null &&
            $this->city != null &&
            $this->postalCode != null &&
            $this->email != null &&
            $this->items != null
        ) {
            return true;
        }
        else {
            return false;
        }
    }



}

?>