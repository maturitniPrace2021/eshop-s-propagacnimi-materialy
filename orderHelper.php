<?php

include("cookieHelper.php");

if(isset($_GET["addItem"]) && isset($_GET["itemId"])) {
    addItemToCart($_GET["itemId"]);
}

if(isset($_GET["removeItem"]) && isset($_GET["itemId"])) {
    removeItemFromCart($_GET["itemId"]);
}
if(isset($_GET["subtractItem"]) && isset($_GET["itemId"])) {
    subtractItemFromCart($_GET["itemId"]);
}



function addItemToCart($itemId) {
    $order = getCurrentOrder();
    $order->add_item($itemId);
    setCurrentOrder($order);
}

function subtractItemFromCart($itemId) {
    $order = getCurrentOrder();
    $order->subtract_item($itemId);
    setCurrentOrder($order);
}

function removeItemFromCart($itemId) {
    $order = getCurrentOrder();
    $order->remove_item($itemId);
    setCurrentOrder($order);
}

?>