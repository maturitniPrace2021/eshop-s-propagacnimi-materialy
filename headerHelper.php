<?php

function getHeaderData() {
    $productCount = getCurrentOrder()->get_product_count();
    $cartProducts = getProductsByIds(getCurrentOrder()->get_product_ids());
    $itemAmounts = getCurrentOrder()->get_items();
    $totalPrice = calculateTotalPrice();
    $headerData["productCount"] = $productCount;
    $headerData["cartProducts"] = $cartProducts;
    $headerData["itemAmounts"] = $itemAmounts;
    $headerData["totalPrice"] = $totalPrice;
    return $headerData;
}

function calculateTotalPrice() {
    $itemAmounts = getCurrentOrder()->get_items();
    $total = 0;
    foreach($itemAmounts as $key => $value) {
        $product = getProductById($key);
        if($product != null) {
            $total += $value * $product->get_price();
        }
    }
    return $total;
}


?>