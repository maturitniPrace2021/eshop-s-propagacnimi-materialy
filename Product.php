<?php

class Product {
    public $id;
    public $name;
    public $description;
    public $category;
    public $price;
    public $inStock;
    public $imgUrl;

    public function __construct($id, $name, $description, $category, $price, $inStock, $imgUrl){
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->category = $category;
        $this->price = $price;
        $this->inStock = $inStock;
        $this->imgUrl = $imgUrl;
    }

    function get_id() {
        return $this->id;
    }

    function get_name() {
        return $this->name;
    }

    function get_description() {
        return $this->description;
    }

    function get_category() {
        return $this->category;
    }

    function get_price() {
        return $this->price;
    }

    function get_inStock() {
        return $this->inStock;
    }

    function get_imgUrl() {
        return $this->imgUrl;
    }
}

?>