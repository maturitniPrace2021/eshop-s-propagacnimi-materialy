<?php

class Category {
    public $id;
    public $name;

    public function __construct($id, $name){
        $this->id = $id;
        $this->name = $name;
    }

    function get_id() {
        return $this->id;
    }

    function get_name() {
        return $this->name;
    }

}

?>