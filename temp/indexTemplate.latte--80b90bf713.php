<?php
// source: templates/indexTemplate.latte

use Latte\Runtime as LR;

class Template80b90bf713 extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>


<div class="container p-5">

   <h1 class="text-center">Oblíbené tiskoviny</h1>

   <hr>

   <div class="row d-flex flex-row justify-content-between">
      <div class="col-lg-2 col-md-3">
<?php
		$iterations = 0;
		foreach ($iterator = $this->global->its[] = new LR\CachingIterator($featuredProducts) as $product) {
			?>            <p class="<?php
			if ($iterator->first) {
				?>featuredActive<?php
			}
			?>" id="featuredProduct<?php echo LR\Filters::escapeHtmlAttr($iterator->counter - 1) /* line 13 */ ?>" style="cursor: pointer;" data-target="#carouselFeatured" data-slide-to="<?php
			echo LR\Filters::escapeHtmlAttr($iterator->counter - 1) /* line 13 */ ?>"><?php echo LR\Filters::escapeHtmlText($product->get_name()) /* line 13 */ ?></p>
<?php
			$iterations++;
		}
		array_pop($this->global->its);
		$iterator = end($this->global->its);
?>
      </div>
      <div class="col-lg-4 col-md-9">
         <div id="carouselFeatured" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
<?php
		$iterations = 0;
		foreach ($iterator = $this->global->its[] = new LR\CachingIterator($featuredProducts) as $product) {
			?>                  <div class="carousel-item <?php
			if ($iterator->first) {
				?>active<?php
			}
?>" style="background-color: white !important; color: black !important;">
                     <a href="http://localhost:8888/plzen-eshop/productDetail.php?i=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($product->get_id())) /* line 21 */ ?>"><h2 class="text-dark"><?php
			echo LR\Filters::escapeHtmlText($product->get_name()) /* line 21 */ ?></h2></a>
                     <p class="cutText-6"><?php echo LR\Filters::escapeHtmlText($product->get_description()) /* line 22 */ ?></p>
                  </div>
<?php
			$iterations++;
		}
		array_pop($this->global->its);
		$iterator = end($this->global->its);
?>
            </div>
         </div>   
      </div>
      <div class="col-lg-4 col-md-12">
         <!-- <div class="bgPrimary"></div> -->
         <img src="https://www.visitplzen.eu/wp-content/uploads/2019/04/suvenry_darky_plzen.jpg" width="100%">
      </div>
   </div>

   <hr>


<?php
		$iterations = 0;
		foreach ($featuredCategories as $category) {
			?>      <a href='http://localhost:8888/plzen-eshop/kategorie.php?c=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($category->get_id())) /* line 38 */ ?>'style="color: black"><h2 class="mt-5"><?php
			echo LR\Filters::escapeHtmlText($category->get_name()) /* line 38 */ ?></h2></a>
      <hr>
      <div class="row">
         <div class="d-none"><?php echo LR\Filters::escapeHtmlText($i = 0) /* line 41 */ ?></div>
<?php
			$iterations = 0;
			foreach ($products as $product) {
				if ($product->get_category() == $category->get_id() && $i < 4) {
					$this->renderBlock('productItem', [$product] + $this->params, 'html');
					?>                  <div class="d-none"><?php echo LR\Filters::escapeHtmlText($i++) /* line 45 */ ?></div>
<?php
				}
?>
            
<?php
				$iterations++;
			}
?>
      </div>
<?php
			$iterations++;
		}
?>

   </div>
</div>

<script>

      $('.carousel').carousel({
         interval: 5000
      })

      $("#carouselFeatured").on('slide.bs.carousel', function(event){
         $("#featuredProduct" + event.from).removeClass("featuredActive");
         $("#featuredProduct" + event.to).addClass("featuredActive");
      });

      function addProductFunc(productId) {
         $.ajax({
               type: 'get',
               dataType: 'html',
               url: '/plzen-eshop/orderHelper.php',
               data: {"itemId": productId, "addItem": true},
               success: function() {
                  $("#headerCart").load(" #headerCart > *");
               },
               complete: function() {
               document.getElementById("cartDropdownBody").addEventListener("click", function(e) {
                  e.stopPropagation();
               });
            }
         });
      }
</script>





<?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['product'])) trigger_error('Variable $product overwritten in foreach on line 12, 19, 42');
		if (isset($this->params['category'])) trigger_error('Variable $category overwritten in foreach on line 37');
		$this->createTemplate("components/blocks.latte", $this->params, "import")->render();
		
	}

}
