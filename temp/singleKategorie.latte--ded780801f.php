<?php
// source: templates/singleKategorie.latte

use Latte\Runtime as LR;

class Templateded780801f extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
		$productCheck = false;
?>

<div class="container p-5">
   <h1 class="text-center"><?php echo LR\Filters::escapeHtmlText($selectedCategory->get_name()) /* line 5 */ ?></h1>

   <hr>   
   
   <form action="" method="GET">
      <input type="text" value="<?php echo LR\Filters::escapeHtmlAttr($selectedCategory->get_id()) /* line 10 */ ?>" class="d-none" id="c" name="c">
      <div class="d-flex flex-row">
         <label for="min" class="my-auto">Cena od:</label>
         <input type="number" min="0" class="form-control w-25 ml-3 mr-5" id="min" name="min" value="<?php
		echo LR\Filters::escapeHtmlAttr($min) /* line 13 */ ?>">
         <label for="max" class="my-auto">Cena do:</label>
         <input type="number" min="0" class="form-control w-25 ml-3 " id="max" name="max" value="<?php echo LR\Filters::escapeHtmlAttr($max) /* line 15 */ ?>">
         <button type="submit" onclick="" class="btn ml-3 bgPrimary white rounded-0">Filtrovat</button>
      </div>
   </form>

   <hr>

   <div class="row">
<?php
		if (isset($products)) {
			$iterations = 0;
			foreach ($products as $product) {
				if (($product->get_price() >= $min)) {
					if (($product->get_price() <= $max)) {
						$this->renderBlock('productItem', [$product] + $this->params, 'html');
						$productCheck = true;
					}
					elseif (($max == null)) {
						$this->renderBlock('productItem', [$product] + $this->params, 'html');
						$productCheck = true;
					}
				}
				$iterations++;
			}
		}
		else {
			$productCheck = true;
?>
   <h3 class="text-center mx-auto">V kategorii se nenachází žádné produkty.</h3>
<?php
		}
?>
   </div>
<?php
		if (($productCheck == false)) {
?>
      <h3 class="text-center mt-5 mb-4">Vašemu filtrování neodpovídají žádné produkty</h3>
<?php
		}
?>
</div>



<?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['product'])) trigger_error('Variable $product overwritten in foreach on line 24');
		$this->createTemplate("components/blocks.latte", $this->params, "import")->render();
		
	}

}
