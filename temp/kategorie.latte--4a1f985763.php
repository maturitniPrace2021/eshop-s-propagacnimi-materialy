<?php
// source: templates/kategorie.latte

use Latte\Runtime as LR;

class Template4a1f985763 extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>


<div class="container p-5">

<h1 class="text-center">Přehled kategorií</h1>



<?php
		$iterations = 0;
		foreach ($iterator = $this->global->its[] = new LR\CachingIterator($categories) as $category) {
			?>      <a href='http://localhost:8888/plzen-eshop/kategorie.php?c=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($category->get_id())) /* line 11 */ ?>'style="color: black"><h2 class="mt-5"><?php
			echo LR\Filters::escapeHtmlText($category->get_name()) /* line 11 */ ?></h2></a>
      <hr>
      <div class="row">
<?php
			if (isset($products[$iterator->counter-1])) {
				$iterations = 0;
				foreach ($iterator = $this->global->its[] = new LR\CachingIterator($products[$iterator->counter-1]) as $product) {
					if ($iterator->counter > 4) break;
					$this->renderBlock('productItem', [$product] + $this->params, 'html');
					$iterations++;
				}
				array_pop($this->global->its);
				$iterator = end($this->global->its);
			}
			else {
?>
         <p class="text-center mx-auto">Kategorie neobsahuje žádné produkty.</p>
<?php
			}
?>
      </div>
<?php
			$iterations++;
		}
		array_pop($this->global->its);
		$iterator = end($this->global->its);
?>
</div>

<?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['product'])) trigger_error('Variable $product overwritten in foreach on line 15');
		if (isset($this->params['category'])) trigger_error('Variable $category overwritten in foreach on line 10');
		$this->createTemplate("components/blocks.latte", $this->params, "import")->render();
		
	}

}
