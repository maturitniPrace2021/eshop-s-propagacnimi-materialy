<?php
// source: templates/kosik.latte

use Latte\Runtime as LR;

class Templatee4ff95ca00 extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<div class="container p-5">   
    <h1 class="mb-0 text-center mb-5">VÁŠ NÁKUPNÍ KOŠÍK</h1>
      <hr>

    <div id="bodyCart" style="" class="p-3 rounded bg-black border-none" aria-labelledby="">

        <div class="d-flex flex-row justify-content-between">
            <div class="d-flex flex-row align-items-start">
                <p class="primary pr-2" style="font-size: 2em;"><?php echo LR\Filters::escapeHtmlText($headerData["productCount"]) /* line 9 */ ?></p>
                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-cart" viewBox="0 0 16 16">
               <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"></path>
            </svg>
            </div>
            <p class="primary" style="font-size: 2em;"><?php echo LR\Filters::escapeHtmlText($headerData["totalPrice"]) /* line 14 */ ?> Kč</p>
        </div>
        <div  class="d-flex flex-column">
            <!-- foreach item v košíku -->
<?php
		if ($headerData["cartProducts"] != null) {
			$iterations = 0;
			foreach ($headerData["cartProducts"] as $product) {
?>
                            <div class="d-flex flex-column flex-lg-row justify-content-between my-3 p-2 border border-primary">
                              <div class="d-flex flex-row align-items-center">
                                 <a class="text-decoration-none black py-auto" style="cursor:pointer;font-size: 2em; color: red;" onclick="removeProductFunc(<?php
				echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($product->get_id())) /* line 22 */ ?>)">×</a>
                                 <div class="my-auto mx-3"
                                    style="width: 50px; height: 50px; <?php
				if ($product->get_imgUrl() == '' || $product->get_imgUrl() == 'unset') {
					?>background-image: url('img/no-image.png');background-repeat: no-repeat;<?php
				}
				else {
					?>background-image: url('storage/<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeCss($product->get_imgUrl())) /* line 24 */ ?>');background-size: cover;<?php
				}
?>  background-position: center;">
                                 </div>
                                 <a href="http://localhost:8888/plzen-eshop/productDetail.php?i=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($product->get_id())) /* line 26 */ ?>" class="my-auto"><h5 class="my-auto cutText-1" style="color: black"><?php
				echo LR\Filters::escapeHtmlText($product->get_name()) /* line 26 */ ?></h5></a>
                                        
                              </div>
                              <div class="d-flex flex-row flex-lg-column justify-content-between">
                                 <p class="mb-0 my-auto text-nowrap text-center"><?php echo LR\Filters::escapeHtmlText($headerData["itemAmounts"][$product->get_id()] * $product->get_price()) /* line 30 */ ?> Kč</p>
                                 <div class="row d-flex flrx-row justify-content-end pl-auto mt-auto mb-0 ml-auto mr-1">
                                    <a onclick="addProductFunc(<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($product->get_id())) /* line 32 */ ?>)" style="cursor:pointer; text-decoration: none; color: black;"><h2 class="mb-0 primary">+</h2></a>
                                    <h5 class="mb-0 px-3 my-auto"><?php echo LR\Filters::escapeHtmlText($headerData["itemAmounts"][$product->get_id()]) /* line 33 */ ?> ks</h5>
                                    <a onclick="subtractProductFunc(<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($product->get_id())) /* line 34 */ ?>)" style="cursor:pointer; text-decoration: none; color: black;"><h2 class="mb-0 primary">-</h2></a>
                                 </div>
                              </div>
                            </div>
<?php
				$iterations++;
			}
?>
                  <div class="d-flex flex-column align-items-center">
                        <a href="objednavka/kontaktni-udaje.php" onclick="">
                            <button type="submit" class="btn rounded-0 btn-outline-danger mx-auto"
                                    style="width:100% !important;cursor:pointer;padding: 5px 20px;">
                                Pokračovat v objednávce
                            </button>
                        </a>
                    </div>
<?php
		}
		else {
?>
                     <hr>
                    <h4 class="text-center">Váš nákupní košík je prázdný.</h4>
<?php
		}
?>
        </div>
    </div>
</div>


<script type="text/javascript">
      function removeProductFunc(productId) {
         $.ajax({
            type: 'get',
            dataType: 'html',
            url: '/plzen-eshop/orderHelper.php',
            data: {"itemId": productId, "removeItem": true},
             success: function() {
               $("#headerCart").load(" #headerCart > *");
               $("#bodyCart").load(" #bodyCart > *");
            },
            complete: function() {
               document.getElementById("cartDropdownBody").addEventListener("click", function(e) {
                  e.stopPropagation();
               });
            }
         });
      }
      function subtractProductFunc(productId) {
         $.ajax({
            type: 'get',
            dataType: 'html',
            url: '/plzen-eshop/orderHelper.php',
            data: {"itemId": productId, "subtractItem": true},
             success: function() {
               $("#headerCart").load(" #headerCart > *");
               $("#bodyCart").load(" #bodyCart > *");
            },
            complete: function() {
               document.getElementById("cartDropdownBody").addEventListener("click", function(e) {
                  e.stopPropagation();
               });
            }
         });
      }
      function addProductFunc(productId) {
         $.ajax({
               type: 'get',
               dataType: 'html',
               url: '/plzen-eshop/orderHelper.php',
               data: {"itemId": productId, "addItem": true},
               success: function() {
                  $("#headerCart").load(" #headerCart > *");
                  $("#bodyCart").load(" #bodyCart > *");
               },
               complete: function() {
               document.getElementById("cartDropdownBody").addEventListener("click", function(e) {
                  e.stopPropagation();
               });
            }
         });
      }

      $( document ).ajaxComplete(function() {
         document.getElementById("cartDropdownBody").addEventListener("click", function(e) {
            e.stopPropagation();
         });
      });
      document.getElementById("cartDropdownBody").addEventListener("click", function(e) {
         e.stopPropagation();
      });
      
</script>



<?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['product'])) trigger_error('Variable $product overwritten in foreach on line 19');
		
	}

}
