<?php
// source: templates/productDetail.latte

use Latte\Runtime as LR;

class Templatec2bcffa353 extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<div class="container px-5">
    <a href="http://localhost:8888/plzen-eshop/kategorie.php?c=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($productCategory->get_id())) /* line 3 */ ?>"><h5 class="text-left my-3 text-muted"><?php
		echo LR\Filters::escapeHtmlText($productCategory->get_name()) /* line 3 */ ?></h5></a>
    <hr>
    <div class="d-flex flex-row row justify-content-between my-4">
        <div class="d-flex flex-column justify-content-start col-lg-4">
            <div class=" m-0" style="height: 300px; <?php
		if ($product->get_imgUrl() == '' || $product->get_imgUrl() == 'unset') {
			?>background-image: url('img/no-image.png');background-repeat: no-repeat;<?php
		}
		else {
			?>background-image: url('storage/<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeCss($product->get_imgUrl())) /* line 7 */ ?>');background-size: cover;<?php
		}
?>  background-position: center;">
            </div>
            <!--
            <div class="d-flex flex-row justify-content-between mt-0 mt-lg-4">
                <div class="bg-primary m-0 mx-auto mx-lg-0 my-4 my-lg-0" style="width: 25%; height: 75px;">
                </div>
                <div class="bg-primary m-0 mx-auto mx-lg-0 my-4 my-lg-0" style="width: 25%; height: 75px;">
                </div>
                <div class="bg-primary m-0 mx-auto mx-lg-0 my-4 my-lg-0" style="width: 25%; height: 75px;">
                </div>
            </div>
            -->
        </div>
        <div class="d-flex flex-column justify-content-between col-lg-4">
            <div class="">
                <h2><?php echo LR\Filters::escapeHtmlText($product->get_name()) /* line 22 */ ?></h2>
                <p class="gray mb-5"><?php echo LR\Filters::escapeHtmlText($product->get_description()) /* line 23 */ ?></p>
            </div>
            <div>
                <div class="d-flex flex-row row justify-content-end mx-0">
                    <h3 class="primary"><?php echo LR\Filters::escapeHtmlText($product->get_price()) /* line 27 */ ?> Kč</h3>
                </div>
                <div class="d-flex flex-row row justify-content-between mx-0">
                    <p class="text-muted my-auto">Skladem: <?php echo LR\Filters::escapeHtmlText($product->get_inStock()) /* line 30 */ ?>ks</p>
<?php
		if (($product->get_inStock() > 0)) {
			?>                    <button onclick="addProductFunc(<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($product->get_id())) /* line 32 */ ?>)" class="btn rounded-0 bgPrimary w-50 py-1 white text-center px-auto">Přidat do košíku</button>
<?php
		}
		else {
?>
                    <p class="rounded-0 bgColor w-50 py-1 white text-center px-auto mb-0">Není skladem</p>
<?php
		}
?>
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="row mb-4">
        <div class="d-none"><?php echo LR\Filters::escapeHtmlText($i = 0) /* line 44 */ ?></div>
<?php
		$iterations = 0;
		foreach ($products as $singleProduct) {
			if ($singleProduct->get_id()!=$product->get_id() && $i < 4) {
				$this->renderBlock('productItem', [$singleProduct] + $this->params, 'html');
				?>                <div class="d-none"><?php echo LR\Filters::escapeHtmlText($i++) /* line 48 */ ?></div>
<?php
			}
			$iterations++;
		}
?>
    </div>
</div><?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['singleProduct'])) trigger_error('Variable $singleProduct overwritten in foreach on line 45');
		$this->createTemplate("components/blocks.latte", $this->params, "import")->render();
		
	}

}
