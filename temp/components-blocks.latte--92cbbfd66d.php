<?php
// source: templates/components/blocks.latte

use Latte\Runtime as LR;

class Template92cbbfd66d extends Latte\Runtime\Template
{
	public $blocks = [
		'productItem' => 'blockProductItem',
	];

	public $blockTypes = [
		'productItem' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		return get_defined_vars();
	}


	function blockProductItem($_args)
	{
		extract($this->params);
		list($product, $onclick) = $_args + [NULL, NULL, ];
?>
    <div class="col-lg-3 col-sm-6 col-10 my-3 mx-auto">
        <a style="text-decoration: none" href="http://localhost:8888/plzen-eshop/productDetail.php?i=<?php
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($product->get_id())) /* line 3 */ ?>">
            <div class= "p-3  border-dark shadow rounded">
                <div class="m-0 mx-auto" style="width: 100%; height: 200px; 
                
                <?php
		if ($product->get_imgUrl() == '' || $product->get_imgUrl() == 'unset') {
			?>background-image: url('img/no-image.png');background-repeat: no-repeat;<?php
		}
		else {
			?>background-image: url('storage/<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeCss($product->get_imgUrl())) /* line 7 */ ?>');background-size: cover;<?php
		}
?>  background-position: center;">
                </div>
                <div class="py-1">
                    <h5 class="text-dark"><?php echo LR\Filters::escapeHtmlText($product->get_name()) /* line 10 */ ?></h5>
                </div>
                <div class="d-flex flex-row row align-items-end mb-2 mx-auto">
                    <h4 class="col p-0 mb-0 text-right primary"><?php echo LR\Filters::escapeHtmlText($product->get_price()) /* line 13 */ ?> Kč</h4>
                </div>
        </a>
                <div class="row">
<?php
		if (($product->get_inStock() > 0)) {
			?>                    <button onclick="addProductFunc(<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::escapeJs($product->get_id())) /* line 18 */ ?>)" class="btn rounded-0 bgPrimary mx-auto py-1 white text-center px-3">Do košíku</button>
<?php
		}
		else {
?>
                    <p class="rounded-0 mx-auto py-1 bgColor white text-center px-3 text-decoration-none">Není skladem</p>
<?php
		}
?>
                </div>
            </div>
    </div>
<?php
	}

}
