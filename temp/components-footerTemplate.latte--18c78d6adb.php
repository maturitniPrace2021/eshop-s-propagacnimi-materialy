<?php
// source: templates/components/footerTemplate.latte

use Latte\Runtime as LR;

class Template18c78d6adb extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<footer class="bg-footer">
<div class="row mx-auto w-100 py-3 d-flex flex-row justify-content-around container">
    <div class="col-lg-3 col-md-6 d-flex flex-column justify-content-start my-auto">
        <a href="https://goo.gl/maps/LTWmjT9geyoimGQ59">
            <div class="mx-0 my-3 d-flex flex-row justify-content-start align-items-center">
                <img src="http://localhost:8888/plzen-eshop/img/bank.svg" style="height: 25px" class="mr-2">
                <p class="mb-0" style="color: black">Náměstí Republiky 41, 301 00 Plzeň</p>
            </div>
        </a>
        <a href="tel:+420378035330">
            <div class="mx-0 my-3 d-flex flex-row justify-content-start align-items-center">
                    <img src="http://localhost:8888/plzen-eshop/img/telephone-fill.svg" style="height: 25px" class="mr-2">
                <p class="mb-0" style="color: black">+420 378 035 330</p>
            </div>
        </a>
        <a href="mailto:info@visitplzen.eu">
            <div class="mx-0 my-3 d-flex flex-row justify-content-start align-items-center">
                <img src="http://localhost:8888/plzen-eshop/img/envelope-fill.svg" style="height: 25px" class="mr-2">
                <p class="mb-0" style="color: black">info@visitplzen.eu</p>
            </div>
        </a>
        <a href="https://www.visitplzen.eu/webkamery/">
            <div class="mx-0 my-3 d-flex flex-row justify-content-start align-items-center">
                <img src="http://localhost:8888/plzen-eshop/img/camera-video-fill.svg" style="height: 25px" class="mr-2">
                <p class="mb-0" style="color: black">Webkamery</p>
            </div>
        </a>
    </div>

    <div class="col-lg-5 col-md-6 d-flex flex-column align-items-center justify-content-start my-auto px-5">
        <p class="text-center primary" style="font-size: 25px;">Navštivte také oficiální stránky<br>města Plzně!</p>
            <a href="https://www.visitplzen.eu/" class="w-50">
                <img class="w-100" src="http://localhost:8888/plzen-eshop/img/logo.svg">
            </a>
    </div>



    <div class="col-lg-4">
        <div class="bgPrimary d-flex flex-column p-3">
            <p class="text-center text-light font-weight-bold" style="font-size: 25px;">Napište nám:</p>
            <form class="d-flex flex-column px-4 contactForm" action="" method="post">
                <input name="email" placeholder="E-mail" pattern="[^ @]*@[^ @]*">
                <input name="predmet" placeholder="Předmět">
                <textarea name="zprava" placeholder="Zpráva"></textarea>
                <button type="submit" name="send" class="mt-3 ml-auto w-25">Odeslat</button>
            </form>
        </div>
    </div>
</div>
</footer>

<?php
		include("mail.php");
		;
?>

<?php
		return get_defined_vars();
	}

}
