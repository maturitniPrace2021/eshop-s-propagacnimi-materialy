<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include("cookieHelper.php");


if(isset($_POST["send"])) {
    send();
}

function send() {
    $name = null;
    $email = null;
    $phoneNumber = null;
    $address = null;
    $city = null;
    $postalCode = null;
    $message = null;

    $errors = [];

    if(trim($_POST["name"]) == null) {
        $errors[] = "Jméno je povinné pole.";
    } else {
        $name = htmlspecialchars($_POST["name"]);
    }

    if(trim($_POST["email"]) == null) {
        $errors[] = "E-mail je povinné pole.";
    } else {
        $email = htmlspecialchars($_POST["email"]);
    }

    if(isset($_POST["phoneNumber"])) {
        $phoneNumber = htmlspecialchars($_POST["phoneNumber"]);
    }

    if(trim($_POST["address"]) == null) {
        $errors[] = "Adresa je povinné pole.";
    } else {
        $address = htmlspecialchars($_POST["address"]);
    }

    if(trim($_POST["city"]) == null) {
        $errors[] = "Město je povinné pole.";
    } else {
        $city = htmlspecialchars($_POST["city"]);
    }

    if(trim($_POST["postalCode"]) == null) {
        $errors[] = "PSČ je povinné pole.";
    } else {
        $postalCode = htmlspecialchars($_POST["postalCode"]);
    }

    if(isset($_POST["message"])) {
        $message = htmlspecialchars($_POST["message"]);
    }

    if(!empty($errors)) {
        echo json_encode($errors);
    } else {
        $order = getCurrentOrder();
        $order->set_nameSubject($name);
        $order->set_email($email);
        $order->set_phoneNumber($phoneNumber);
        $order->set_address($address);
        $order->set_city($city);
        $order->set_postalCode($postalCode);
        $order->set_message($message);
        setCurrentOrder($order);
        echo(json_encode("VALID"));
    }
}








?>