-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 15, 2021 at 12:24 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `turismus`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `name`, `password`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997');

-- --------------------------------------------------------

--
-- Table structure for table `featured_categories`
--

CREATE TABLE `featured_categories` (
  `id` int(11) NOT NULL,
  `id_category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `featured_categories`
--

INSERT INTO `featured_categories` (`id`, `id_category`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `featured_products`
--

CREATE TABLE `featured_products` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `featured_products`
--

INSERT INTO `featured_products` (`id`, `id_product`) VALUES
(1, 1),
(2, 2),
(4, 8);

-- --------------------------------------------------------

--
-- Table structure for table `kategorie`
--

CREATE TABLE `kategorie` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `kategorie`
--

INSERT INTO `kategorie` (`id`, `name`) VALUES
(1, 'Hrnečky PLZEŇ'),
(3, 'Tiskoviny PLZEŇ'),
(4, 'Prázdná kategorie 2');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `totalprice` double NOT NULL,
  `subjectName` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `subjectAddress` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `subjectCity` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `subjectPostalCode` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `subjectMail` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `subjectPhone` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `notes` varchar(255) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `date`, `totalprice`, `subjectName`, `subjectAddress`, `subjectCity`, `subjectPostalCode`, `subjectMail`, `subjectPhone`, `notes`) VALUES
(1, '2021-04-15 13:19:15', 2396, 'Denis', 'Nevim', 'Nevim', '123 45', 'muj@email.cz', '158', '');

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`id`, `order_id`, `item`, `count`) VALUES
(1, 1, 2, 2),
(2, 1, 10, 2);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `description` text COLLATE utf8_czech_ci NOT NULL,
  `category` int(11) NOT NULL,
  `price` double NOT NULL,
  `inStock` int(11) NOT NULL,
  `imgUrl` text COLLATE utf8_czech_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `category`, `price`, `inStock`, `imgUrl`) VALUES
(1, 'Plecháček v Plzni na Plech', 'Hezký praktický suvenýr, za který Vás po návratu z Plzně pochválí celá domácnost. Durabilní plecháček V Plzni na Plech je vhodný i do myčky.', 1, 299, 20, '1.jpg'),
(2, 'Hrneček I ❤️ #PLZEŇ', 'S tímto vlasteneckým hrníčkem bude hned každý vědět, kam patří Vaše srdíčko a ve které lihovině by se nejraději utopilo. Hrneček vhodný na každou obří párty, nebo klidně jen tak k domácímu popíjení. Obsah hrnečku by vždy měla být Plzeňská dvanáctka, ale v případě nouze připadá v úvahu i čaj.', 1, 249, 15, '2.png'),
(4, 'Tiskovina DEPO2015', 'Všechny akce konané v DEPO2015, nejen v roce 2015.', 3, 20, 200, '4.png'),
(5, 'Tiskovina HUB 2.0', 'Soupis akcí konané v areálu HUB 2.0 v roce 2021', 3, 15, 0, '5.png'),
(6, 'Další plecháček', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque arcu. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui. Aliquam erat volutpat. Nullam dapibus fermentum ipsum. Curabitur bibendum justo non orci. Nam quis nulla. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam sapien ', 1, 200, 30, '6.jpg'),
(7, 'Další hrneček', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque arcu. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui. Aliquam erat volutpat. Nullam dapibus fermentum ipsum.', 1, 349, 5, '7.jpg'),
(8, 'Tiskovina SPŠE', 'Soupiska akcí konaných na SPŠE.', 3, 5, 420, '8.png'),
(9, 'Další tiskovina', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque arcu. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui. Aliquam erat volutpat. Nullam dapibus fermentum ipsum. Curabitur bibendum justo non orci. Nam quis nulla. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam sapien ', 3, 1200, 4, '9.png'),
(10, 'Podtácek Techheaven', 'Stylový podtácek pro moderní inovativní IT firmu.\n***Nepoužitý***', 3, 949, 4, '10.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `featured_categories`
--
ALTER TABLE `featured_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `featured_products`
--
ALTER TABLE `featured_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategorie`
--
ALTER TABLE `kategorie`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `featured_categories`
--
ALTER TABLE `featured_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `featured_products`
--
ALTER TABLE `featured_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kategorie`
--
ALTER TABLE `kategorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
