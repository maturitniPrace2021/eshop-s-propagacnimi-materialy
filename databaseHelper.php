<?php
    include("Product.php");
    include("Order.php");
    include("Category.php");
    include("config.php");

    function getProductsByCategory($categoryId) {
        $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
        if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
        }
    
        $sql = "SET CHARACTER SET UTF8";
        $result = $conn->query($sql);
    
        $sql = "SELECT * FROM products WHERE category='$categoryId'";
        $result = $conn->query($sql);
    
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
            $products[] = new Product($row["id"],$row["name"], $row["description"], $row["category"], $row["price"], $row["inStock"], $row["imgUrl"]);
            }
          }
        else {
            echo "<script type='text/javascript'>console.log('0 výsledků z databáze');</script>";
        }
        return $products;
        $conn->close();
    }

    function getProductsBySearchName($name) {
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
  
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "SELECT * FROM products WHERE name LIKE '%".$name."%'";
      $result = $conn->query($sql);
  
      if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
          $products[] = new Product($row["id"],$row["name"], $row["description"], $row["category"], $row["price"], $row["inStock"], $row["imgUrl"]);
          }
        }
      else {
      }
      return $products;
      $conn->close();
  }

    function getProductById($productId) {
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
        if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
        }
    
        $sql = "SET CHARACTER SET UTF8";
        $result = $conn->query($sql);
    
        $sql = "SELECT * FROM products WHERE id='$productId'";
        $result = $conn->query($sql);
    
        $product = null;

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
            $product = new Product($row["id"],$row["name"], $row["description"], $row["category"], $row["price"], $row["inStock"], $row["imgUrl"]);
            }
          }
        else {
            echo "<script type='text/javascript'>console.log('0 výsledků z databáze');</script>";
        }
        return $product;
        $conn->close();
    }

    function checkAdminLogin($name, $password) {
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
        if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
        }
    
        $sql = "SET CHARACTER SET UTF8";
        $result = $conn->query($sql);

        $password = sha1($password);
    
        $sql = "SELECT * FROM accounts WHERE name='$name' AND password='$password'";
        $result = $conn->query($sql);
    
        $loggedIn = false;

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
              $loggedIn = true;
            }
          }
        else {
            $loggedIn = false;
        }
        $conn->close();
        return $loggedIn;
    }

    function getProductsByIds($productIds) {
      if ($productIds != null) {
        foreach ($productIds as $productId) {
          $products[] = getProductById($productId);
        }
        return $products;
      } else {return null;}
    }

    function getProducts() {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "SELECT * FROM products";
      $result = $conn->query($sql);
  
      if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            $products[] = new Product($row["id"],$row["name"], $row["description"], $row["category"], $row["price"], $row["inStock"], $row["imgUrl"]);
          }
        }
      else {
          echo "<script type='text/javascript'>console.log('0 výsledků z databáze');</script>";
      }
      return $products;
      $conn->close();
    }

    function getFeaturedProducts() {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "SELECT * FROM featured_products";
      $result = $conn->query($sql);
  
      if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            $productIds[] = $row["id_product"];
          }
        }
      else {
          echo "<script type='text/javascript'>console.log('0 výsledků z databáze');</script>";
      }
      return getProductsByIds($productIds);
      $conn->close();
    }

    function getFeaturedCategories() {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "SELECT * FROM featured_categories";
      $result = $conn->query($sql);
  
      if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            $categoryIds[] = $row["id_category"];
          }
        }
      else {
          echo "<script type='text/javascript'>console.log('0 výsledků z databáze');</script>";
      }
      return getCategoriesByIds($categoryIds);
      $conn->close();
    }

    function insertFeaturedCategory($id) {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "INSERT INTO featured_categories(id_category) VALUES('$id')";
      $result = $conn->query($sql);
      return $conn->insert_id;
      $conn->close();
    }

    function insertFeaturedProduct($id) {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "INSERT INTO featured_products(id_product) VALUES('$id')";
      $result = $conn->query($sql);
      return $conn->insert_id;
      $conn->close();
    }

    function removeFeaturedCategory($id) {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "DELETE FROM featured_categories WHERE id_category=$id";
      $result = $conn->query($sql);

      $conn->close();
    }

    function removeFeaturedProduct($id) {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "DELETE FROM featured_products WHERE id_product=$id";
      $result = $conn->query($sql);

      $conn->close();
    }

    function getCategoriesByIds($categoryIds) {
      $categories = null;
      foreach($categoryIds as $categoryId) {
        $categories[] = getCategoryById($categoryId);
      }
      return $categories;
    }

    function getCategories() {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "SELECT * FROM kategorie";
      $result = $conn->query($sql);
  
      if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
          $categories[] = new Category($row["id"], $row["name"]);
          }
        }
      else {
          echo "<script type='text/javascript'>console.log('0 výsledků z databáze');</script>";
      }
      return $categories;
      $conn->close();
    }

    function getCategoryById($categoryId) {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "SELECT * FROM kategorie WHERE id='$categoryId'";
      $result = $conn->query($sql);
  
      if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
          $category = new Category($row["id"], $row["name"]);
          }
        }
      else {
          echo "<script type='text/javascript'>console.log('0 výsledků z databáze');</script>";
      }
      return $category;
      $conn->close();
    }

    function insertProduct($name, $description, $category, $price, $inStock, $imgUrl) {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "INSERT INTO products(name,description,category,price,inStock,imgUrl) VALUES('$name','$description','$category','$price','$inStock', '$imgUrl')";
      $result = $conn->query($sql);
      return $conn->insert_id;

      $conn->close();
    }

    function insertOrder($items, $nameSubject, $address, $city, $postalCode, $email, $phoneNumber, $message) {

      
      $totalPrice = 0;
      foreach($items as $key => $value) {
          $product = getProductById($key);
          if($product != null) {
              $totalPrice += $value * $product->get_price();
          }
      }
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "INSERT INTO orders(totalprice, subjectName, subjectAddress, subjectCity, subjectPostalCode, subjectMail, subjectPhone, notes) VALUES('$totalPrice', '$nameSubject', '$address', '$city', '$postalCode', '$email', '$phoneNumber', '$message')";
      $result = $conn->query($sql);
      $addedId = $conn->insert_id;

      foreach($items as $key => $value) {
        $sql = "INSERT INTO order_products(order_id, item, count) VALUES('$addedId', '$key', '$value')";
        $result = $conn->query($sql);
      }

      return true;
      $conn->close();
    }

    function getOrders() {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "SELECT * FROM orders";
      $result = $conn->query($sql);

      $orders = null;
  
      if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            $sql = "SELECT * FROM order_products WHERE order_id=$row[id]";
            $result1 = $conn->query($sql);
            $items = null;
            if ($result1->num_rows > 0) {
                while($row1 = $result1->fetch_assoc()) {
                $items[$row1["item"]] = $row1["count"] ;
                }
              }
            else {
                echo("Nelze najít produkty asociované s objednávkou.");
            }
          $orders[] = new Order($row["id"], $row["totalprice"], $row["subjectName"], $row["subjectMail"], $row["subjectPhone"], $row["subjectAddress"], $row["subjectCity"], $row["subjectPostalCode"], $row["notes"], $items);
          }
        }
      else {
          echo "<script type='text/javascript'>console.log('0 výsledků z databáze');</script>";
      }
      return $orders;
      $conn->close();
    }

    function getOrderById($id) {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "SELECT * FROM orders WHERE id='$id'";
      $result = $conn->query($sql);

      $order = null;
  
      if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            $sql = "SELECT * FROM order_products WHERE order_id=$row[id]";
            $result1 = $conn->query($sql);
            $items = null;
            if ($result1->num_rows > 0) {
                while($row1 = $result1->fetch_assoc()) {
                $items[$row1["item"]] = $row1["count"] ;
                }
              }
            else {
                echo("Nelze najít produkty asociované s objednávkou.");
            }
          $order = new Order($row["id"], $row["totalprice"], $row["subjectName"], $row["subjectMail"], $row["subjectPhone"], $row["subjectAddress"], $row["subjectCity"], $row["subjectPostalCode"], $row["notes"], $items);
          }
        }
      else {
          echo "<script type='text/javascript'>console.log('0 výsledků z databáze');</script>";
      }
      return $order;
      $conn->close();
    }

    function insertCategory($name) {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "INSERT INTO kategorie(name) VALUES('$name')";
      $result = $conn->query($sql);
      return $conn->insert_id;
      $conn->close();
    }

    function updateCategory($id, $name) {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "UPDATE kategorie SET name='$name' WHERE id=$id";
      $result = $conn->query($sql);

      $conn->close();
    }

    function deleteCategory($id) {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "DELETE FROM kategorie WHERE id=$id";
      $result = $conn->query($sql);

      $conn->close();
    }

    function updateProduct($name, $description, $category, $price, $inStock, $id) {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "UPDATE products SET name='$name', description='$description', category='$category', price='$price', inStock='$inStock' WHERE id=$id";
      $result = $conn->query($sql);
      return $conn->insert_id;

      $conn->close();
    }

    function updateProductImg($id, $extension) {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "UPDATE products SET imgUrl='$id$extension' WHERE id=$id";
      $result = $conn->query($sql);
  
      $conn->close();
    }

    function updateItemCount($id, $newCount) {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "UPDATE products SET inStock='$newCount' WHERE id=$id";
      $result = $conn->query($sql);
  
      $conn->close();
    }

    function removeProductImg($id) {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "UPDATE products SET imgUrl='unset' WHERE id=$id";
      $result = $conn->query($sql);

      $conn->close();
    }

    function deleteProduct($id) {
      
      $conn = new mysqli(DBSERVERNAME, DBUSERNAME, DBPASSWORD, DBNAME);
    
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
  
      $sql = "SET CHARACTER SET UTF8";
      $result = $conn->query($sql);
  
      $sql = "DELETE FROM products WHERE id=$id";
      $result = $conn->query($sql);

      $conn->close();
    }
?>